$Policy = "RemoteSigned"
If ((get-ExecutionPolicy) -ne $Policy) {
  Write-Host "Script Execution is disabled. Enabling it now"
  Set-ExecutionPolicy $Policy -Force
  Write-Host "Please Re-Run this script in a new powershell enviroment"
  Exit
}
#  开始安装IIS
$iisInstallPro = "Web-Filtering","Web-IP-Security","Web-Url-Auth",
"Web-Windows-Auth","Web-Basic-Auth","Web-CertProvider","Web-Http-Errors","Web-Static-Content",
"Web-Default-Doc","Web-Dir-Browsing","Web-Http-Redirect","Web-Stat-Compression","Web-Dyn-Compression","Web-Http-Logging",
"Web-Log-Libraries","Web-Custom-Logging","Web-ASP","Web-Asp-Net","Web-Asp-Net45","Web-CGI","Web-ISAPI-Ext","Web-ISAPI-Filter",
"Web-AppInit","Web-Mgmt-Console","Web-WHC",
"Web-WebSockets","Web-Includes";
$features =get-windowsfeature web-*
foreach($item in $features)
{
　　if($item.installed -eq $false -and $iisInstallPro -Match $item.Name)
    {
       Write-Host "Install:" $item.displayname
　　　　$item | add-windowsfeature -WarningAction silentlyContinue
    }
}
# --------------------------------------------------------------------
# Define the variables.
# --------------------------------------------------------------------
$InetPubRoot = "D:\SCM7"
$InetPubLog = "D:\Inetpub\Log"
# --------------------------------------------------------------------
# Define the variables.
# --------------------------------------------------------------------
$InetPubRoot = "D:\SCM7"
$InetPubLog = "D:\Inetpub\Log"


# --------------------------------------------------------------------
# Loading Feature Installation Modules
# --------------------------------------------------------------------
Import-Module ServerManager 


# --------------------------------------------------------------------
# Loading IIS Modules
# --------------------------------------------------------------------
Import-Module WebAdministration

# --------------------------------------------------------------------
# Creating IIS Folder Structure
# --------------------------------------------------------------------
New-Item -Path $InetPubRoot -type directory -Force -ErrorAction SilentlyContinue
New-Item -Path $InetPubLog -type directory -Force -ErrorAction SilentlyContinue


# --------------------------------------------------------------------
# Setting directory access
# --------------------------------------------------------------------
$Command = "icacls $InetPubRoot /grant BUILTIN\IIS_IUSRS:(OI)(CI)(RX) BUILTIN\Users:(OI)(CI)(RX)"
cmd.exe /c $Command
$Command = "icacls $InetPubLog /grant ""NT SERVICE\TrustedInstaller"":(OI)(CI)(F)"
cmd.exe /c $Command

# --------------------------------------------------------------------
# Setting IIS Variables
# --------------------------------------------------------------------
#Changing Log Location
$Command = "%windir%\system32\inetsrv\appcmd set config -section:system.applicationHost/sites -siteDefaults.logfile.directory:$InetPubLog"
cmd.exe /c $Command
$Command = "%windir%\system32\inetsrv\appcmd set config -section:system.applicationHost/log -centralBinaryLogFile.directory:$InetPubLog"
cmd.exe /c $Command
$Command = "%windir%\system32\inetsrv\appcmd set config -section:system.applicationHost/log -centralW3CLogFile.directory:$InetPubLog"
cmd.exe /c $Command
$Command = "%windir%\system32\inetsrv\appcmd add apppool /in < C:\Program Files (x86)\quantum\apppools.xml"
cmd.exe /c $Command
$Command = "%windir%\system32\inetsrv\appcmd add apppool /in < C:\Program Files (x86)\quantum\apppools.xml"