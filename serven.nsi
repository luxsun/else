; 该脚本使用 HM VNISEdit 脚本编辑器向导产生

; 安装程序初始定义常量
!define PRODUCT_NAME "quantuminit"
!define PRODUCT_VERSION "2.0"
!define PRODUCT_PUBLISHER "Qt-Asia."
!define PRODUCT_WEB_SITE "http://www.qt-asia.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\installiis.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

SetCompressor lzma

; ------ MUI 现代界面定义 (1.67 版本以上兼容) ------
!include "MUI.nsh"

; MUI 预定义常量
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; 欢迎页面
!insertmacro MUI_PAGE_WELCOME
; 许可协议页面
!insertmacro MUI_PAGE_LICENSE "test.txt"
; 组件选择页面
!insertmacro MUI_PAGE_COMPONENTS
; 安装过程页面
!insertmacro MUI_PAGE_INSTFILES
; 安装完成页面
!insertmacro MUI_PAGE_FINISH

; 安装卸载过程页面
!insertmacro MUI_UNPAGE_INSTFILES

; 安装界面包含的语言设置
!insertmacro MUI_LANGUAGE "SimpChinese"

; 安装预释放文件
!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS
; ------ MUI 现代界面定义结束 ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "quantum.exe"
InstallDir "C:\quantum"
InstallDirRegKey HKLM "${PRODUCT_UNINST_KEY}" "UninstallString"
ShowInstDetails show
ShowUnInstDetails show
BrandingText "QuantumInit"

Section "InstallIIS" SEC01
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
  File "sites.xml"
  File "installiis.exe"
  File "apppools.xml"
  ExecWait '"$INSTDIR\installiis.exe" '
SectionEnd

Section "InstallSoftWare" SEC02
  File "winrar-x64-560scp.exe"
  File "rewrite_amd64_zh-CN.msi"
  File "requestRouter_amd64.msi"
  File "npp.7.8.6.Installer.x64.exe"
  File "FileZilla_Server.exe"
  File "ChromeStandaloneSetup64.exe"
  ExecWait '"msiexec" /package "rewrite_amd64_zh-CN.msi"  /qn /l* "$INSTDIR\rewrite_amd64_zh-CN.msi.log'
  ExecWait '"msiexec" /package "requestRouter_amd64.msi"  /qn /l* "$INSTDIR\requestRouter_amd64.msi.log'
  ExecWait '"$INSTDIR\npp.7.8.6.Installer.x64.exe"  /S /q'
  ExecWait '"$INSTDIR\winrar-x64-560scp.exe"  /S /q'
  ExecWait '"$INSTDIR\ChromeStandaloneSetup64.exe"  /S /q'
  ExecWait '"$INSTDIR\FileZilla_Server.exe"  /S /q'
SectionEnd

Section -AdditionalIcons
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateDirectory "$SMPROGRAMS\quantuminit"
  CreateShortCut "$SMPROGRAMS\quantuminit\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut "$SMPROGRAMS\quantuminit\Uninstall.lnk" "$INSTDIR\uninst.exe"
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\installiis.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\installiis.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd

#-- 根据 NSIS 脚本编辑规则，所有 Function 区段必须放置在 Section 区段之后编写，以避免安装程序出现未可预知的问题。--#

; 区段组件描述
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC01} ""
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC02} ""
!insertmacro MUI_FUNCTION_DESCRIPTION_END

/******************************
 *  以下是安装程序的卸载部分  *
 ******************************/

Section Uninstall
  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\uninst.exe"
  Delete "$INSTDIR\ChromeStandaloneSetup64.exe"
  Delete "$INSTDIR\FileZilla_Server.exe"
  Delete "$INSTDIR\npp.7.8.6.Installer.x64.exe"
  Delete "$INSTDIR\requestRouter_amd64.msi"
  Delete "$INSTDIR\rewrite_amd64_zh-CN.msi"
  Delete "$INSTDIR\winrar-x64-560scp.exe"
  Delete "$INSTDIR\apppools.xml"
  Delete "$INSTDIR\installiis.exe"
  Delete "$INSTDIR\sites.xml"

  Delete "$SMPROGRAMS\quantuminit\Uninstall.lnk"
  Delete "$SMPROGRAMS\quantuminit\Website.lnk"

  RMDir "$SMPROGRAMS\quantuminit"

  RMDir "$INSTDIR"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd

#-- 根据 NSIS 脚本编辑规则，所有 Function 区段必须放置在 Section 区段之后编写，以避免安装程序出现未可预知的问题。--#

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "您确实要完全移除 $(^Name) ，及其所有的组件？" IDYES +2
  Abort
FunctionEnd

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) 已成功地从您的计算机移除。"
FunctionEnd
